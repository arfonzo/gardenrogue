
import net.slashie.libjcsi.wswing.WSwingConsoleInterface;
import net.slashie.libjcsi.ConsoleSystemInterface;
import net.slashie.libjcsi.CSIColor;
import net.slashie.libjcsi.CharKey;
import net.slashie.util.Util;
import java.util.List;
import java.util.ArrayList;
import java.util.Properties;

public class GardenRogue {
	private static int SCREEN_ROWS = 25;
	private static int SCREEN_COLS = 80;

	private static int viewOffsetX = 1;
	private static int viewOffsetY = 1;

	static int plotSizeX = 40;
	static int plotSizeY = 20;

	static int maxPlotSizeX = 60;
	static int maxPlotSizeY = 24;

	static boolean debugMode = true;	
	private static ConsoleSystemInterface csi = null;
	private static Plot plot;
	private static Gardener gardener;
	private static boolean isPlaying = true;
	static List<Plant> plants = new ArrayList<Plant>();

    public static final void main(String[] args) {
    	try {    		
			Properties text = new Properties();
			text.setProperty("fontSize","20");
			text.setProperty("font", "Courier");

            csi = new WSwingConsoleInterface("GardenRogue", text);
        } catch (ExceptionInInitializerError eiie) {
            System.out.println("Error Initializing Swing:");
            eiie.printStackTrace();
            System.exit(-1);
        }        

		// Title screen
        drawTitle();

		// Load stuff
		loadPlants();
		loadPlot();
		loadGardener();

		// Draw character stats
		drawGardenerStats();		

		// Main in-game loop
		mainLoop();   	

		// Exit.
		csi.cls();
		System.exit(0);
    }

	private static void drawGardener() {
		csi.print(gardener.x+viewOffsetX, gardener.y+viewOffsetY, '@', CSIColor.HOLLYWOOD_CERISE);
	}

	private static void drawGardenerStats() {
		// TODO: Randomise Gardener's history.
		csi.cls();		
    	csi.print(0,0, "GARDENER", CSIColor.EMERALD);

    	csi.print(2,2, "Strength:", CSIColor.FERN_GREEN);
		csi.print(20,2, String.valueOf(gardener.str), CSIColor.WHITE);

		csi.print(0,8, "PLOT", CSIColor.EMERALD);

		csi.print(2,10, "Allotment size:", CSIColor.FERN_GREEN);
		csi.print(20,10, plotSizeX+"x"+plotSizeY+"m", CSIColor.WHITE);

		csi.print(2,11, "Average soil PH:", CSIColor.FERN_GREEN);
		csi.print(20,11, String.format("%.3f",plot.getAveragePH()), CSIColor.WHITE);
    	
		csi.print(4,14, "Born to lowly round-eyes, you were bequeathed nothing upon your parents'", CSIColor.JADE);
		csi.print(4,15, "deaths, except for a rusty shovel and pitchfork. One day, you receive", CSIColor.JADE);
		csi.print(4,16, "a call from the local allotment. Seems your mother registered for the", CSIColor.JADE);
		csi.print(4,17, "waiting list over two decades ago:", CSIColor.JADE);

		csi.print(4,19, "\"Mr. Smith's kicked the bucket, freeing up the plot for you.\" Fantastic!", CSIColor.JADE);

		csi.print(8,21, "Your sense of adventure and civic duty get the best of you...", CSIColor.CORNFLOWER_BLUE);

		csi.print(22,23, "Press any key to enter GardenRogue", CSIColor.FRENCH_ROSE);

    	csi.refresh();
    	csi.inkey();    	
    	csi.cls();
	}
    
    private static void drawHelp() {
    	csi.cls();
    	csi.print(0,0, "HELP", CSIColor.EMERALD);
    	csi.print(20,12, "Sorry, you're on your own for now.", CSIColor.FERN_GREEN);
		csi.print(20,16, "Oh, press ESC to quit.", CSIColor.FERN_GREEN);
    	
    	csi.refresh();
    	csi.inkey();    	
    	csi.cls();
    }

	private static void drawPlot() {
		char[][] map = plot.getMap();
		Land[][] land = plot.getLand();	

		for (int y = 0; y < map.length; y++) {
			for (int x = 0; x < map[y].length; x++) {
				// Check for plant.
				if (land[y][x].plant == null) {
					csi.print(x+viewOffsetX, y+viewOffsetY, map[y][x], CSIColor.KHAKI);
				} else {
					char c = '!';

					if (land[y][x].plant.energy <= 5) {
						csi.print(x+viewOffsetX, y+viewOffsetY, c, CSIColor.ARMY_GREEN);
					} else if (land[y][x].plant.energy <= 10) {
						csi.print(x+viewOffsetX, y+viewOffsetY, c, CSIColor.SEA_GREEN);
					} else if (land[y][x].plant.energy <= 15) {
						csi.print(x+viewOffsetX, y+viewOffsetY, c, CSIColor.LAWN_GREEN);
					} else {
						csi.print(x+viewOffsetX, y+viewOffsetY, c, CSIColor.BRIGHT_GREEN);
					}
					
				}
			}
		}
	}

	private static void drawTitle() {
		csi.cls();

		csi.print(2,5, "GardenRogue", CSIColor.AMETHYST);
		csi.print(4,10, "By Arfonzo J. Coward <arfonzo@gmail.com>", CSIColor.ATOMIC_TANGERINE);

		csi.print(29,23, "Press any key to start", CSIColor.FRENCH_ROSE);

		csi.refresh();
    	csi.inkey();    	
    	csi.cls();
		csi.refresh();
	}

	private static boolean isFree(int x, int y) {
		return true;
	}

	private static void loadGardener() {
		gardener = new Gardener();

		gardener.rollStats();

		if (GardenRogue.debugMode) System.out.println("Gardener stats\tstr:" + gardener.str);
	}

	/* Load all plants */
	public static boolean loadPlants() {
		//if (GardenRogue.debugMode) System.out.println("TODO: loadPlants()");

		Fern fern = new Fern();
		GardenRogue.plants.add(fern);
		if (GardenRogue.debugMode) System.out.println("Fern:" + fern + "\tplants: " + GardenRogue.plants);

		return true;
	}

	private static void loadPlot() {
		plot = new Plot(plotSizeX, plotSizeY);

		/*
		Land[][] land = plot.getLand();

		for (int y = 0; y < land.length; y++) {
			for (int x = 0; x < land[y].length; x++) {
				if (GardenRogue.debugMode) System.out.println("land["+y+"]["+x+"]: " + land[y][x].plotX + "x" + land[y][x].plotY + "\tAlt: " + land[y][x].getAltitude() + "\tSoil PH: "+ land[y][x].soilPH);
			}
		}
		*/
	}

	private static void lookAt(int x, int y) {
		Land[][] land = plot.getLand();

		if (GardenRogue.debugMode) System.out.println("Looking at " + x + "x" + y + ": " + land[y][x].soilPH);

		csi.print(61, 0, "Land Properties", CSIColor.GREEN);

		csi.print(61, 2, " PH:", CSIColor.FLAX);
		csi.print(66, 2, String.format("%.1f", land[y][x].soilPH), CSIColor.WHITE);

		csi.print(61, 3, "Alt:", CSIColor.FERN_GREEN);
		csi.print(66, 3, land[y][x].altitude + "cm", CSIColor.WHITE);

		csi.print(61, 4, "H\u20820:", CSIColor.LILAC);
		csi.print(66, 4, land[y][x].moisture + "u", CSIColor.WHITE);

		if (land[y][x].plant != null) {
			csi.print(61, 12, "Plant Properties", CSIColor.GREEN);
			csi.print(61, 14, land[y][x].plant.name, CSIColor.EMERALD);

			csi.print(61, 15, " HP:", CSIColor.FERN_GREEN);
			csi.print(66, 15, String.valueOf(land[y][x].plant.energy) + "/" + String.valueOf(land[y][x].plant.health), CSIColor.WHITE);

			csi.print(61, 16, "Siz:", CSIColor.FERN_GREEN);
			csi.print(66, 16, String.valueOf((double)(land[y][x].plant.size)/10) + "cm", CSIColor.WHITE);
		}

		//csi.refresh();
	}

	private static void mainLoop() {	

		while (isPlaying) {
			// Draw current state
			csi.cls();
			drawPlot();
			drawGardener();
			lookAt(gardener.x, gardener.y);

			// Refresh display
			csi.refresh();

			net.slashie.libjcsi.CharKey key = csi.inkey();
        	int i = key.code;
        	
			if (debugMode) System.out.println("KEY: " + i + " " + key);
			char[][] map;

			switch (i) {
        		case 0:
        			// Up
        			if (gardener.y > 0 && isFree(gardener.x, gardener.y-1) ) {
        				gardener.y--;
						lookAt(gardener.x, gardener.y);
        			}
        			break;
        			
        		case 1:
        			// Down
					map = plot.getMap();
        			if (gardener.y < map.length - 1 && isFree(gardener.x, gardener.y+1) && gardener.y < SCREEN_ROWS - 1 ) {
               			gardener.y++;
						lookAt(gardener.x, gardener.y);
        			}
        			break;
        			
        		case 2:
        			// Left
        			if (gardener.x > 0 && isFree(gardener.x-1, gardener.y)) {
        				gardener.x--;
						lookAt(gardener.x, gardener.y);
        			}     			
        			break;
        		
        		case 3:
        			// Right
					map = plot.getMap();
        			if (gardener.x < map[0].length - 1 && isFree(gardener.x+1, gardener.y) && gardener.x < SCREEN_COLS - 1 ) {
        				gardener.x++;
						lookAt(gardener.x, gardener.y);
					}		
        			break;

				case 30:
					// ESC: quit
					isPlaying = false;
					break;
        			
        		case 62:
        			// ?: help
        			drawHelp();
        			break;
        			
        		case 93:
        			// D: debug
        			if (debugMode) {
        				debugMode = false;
        			} else {
        				debugMode = true;
        			}
        			break;
        		
				case 101:
					// L: look
					lookAt(gardener.x, gardener.y);
					break;

        		default:
        			break;

			} // end switch

		} // end while

	} // end mainLoop()
    
}
