GardenRogue
===========

About
-----

GardenRogue is a gardening simulation, with a roguelike twist.

Note that this is project is incomplete, and currently under development. It will not always run. 

In fact, you probably shouldn't even try to do anything with this repo, for now.

You have been warned.

Install
-------

You need libjcsi.
- Download, and add 'libjcsi.jar' into the 'GardenRogue' directory (where 'GardenRogue.java' is located).

### Compiling

`
javac -cp ".;.\libjcsi.jar" GardenRogue.java
`

### Running

`
java -cp ".;.\libjcsi.jar" GardenRogue
`

