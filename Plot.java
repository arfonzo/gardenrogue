import java.util.Arrays;
import java.util.List;

public class Plot {

	private static Land[][] land;

	static int sizeX = 40;
	static int sizeY = 20;

	public Plot(int size_x, int size_y) {

		if (size_x > GardenRogue.maxPlotSizeX) {
			if (GardenRogue.debugMode) System.out.println("Warning: Plot X too large - setting to GardenRogue.maxPlotSizeX!");
			sizeX = GardenRogue.maxPlotSizeX;
		} else {
			sizeX = size_x;
		}

		if (size_y > GardenRogue.maxPlotSizeY) {
			if (GardenRogue.debugMode) System.out.println("Warning: Plot Y too large - setting to GardenRogue.maxPlotSizeY!");
			sizeY = GardenRogue.maxPlotSizeY;
		} else { 
			sizeY = size_y;
		}


		land = new Land[sizeY][sizeX];

		for (int y = 0; y < land.length; y++) {
			for (int x = 0; x < land[y].length; x++) {
				land[y][x] = new Land(x,y);
			}
		}		

		//if (GardenRogue.debugMode) System.out.println("New Plot Object created.");
	}

	public double getAveragePH() {
		int size = GardenRogue.plotSizeX * GardenRogue.plotSizeY;
		//if (GardenRogue.debugMode) System.out.println("plot size: " + size);
		double total = 0.0;

		for (int y = 0; y < land.length; y++) {
			for (int x = 0; x < land[y].length; x++) {
				//map[y][x] = '.';
				total += land[y][x].soilPH; 
			}
		}

		//if (GardenRogue.debugMode) System.out.println("plot PH total: " + total);

		return total/size;
	}

	public Land[][] getLand() {
		return land;
	}

	/* Return a 2D Char array representing the Plot. */
	public char[][] getMap() {

		char[][] map = new char[land.length][land[0].length];

		for (int y = 0; y < land.length; y++) {
			for (int x = 0; x < land[y].length; x++) {
				map[y][x] = '.'; 
			}
		}

		return map;
	}	

	public Plot getPlot() {
		return this;
	}
}