import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Plant {
	String name = "Unnamed Plant";
	int x = -1;
	int y = -1;
	int health = 0;
	int energy = health;
	int moisture = 0;
	int size = 0;

	/* NPK that Plant currently needs
		I.e., 20-20-20: take 20 -> 1 to 5 ratio out of 100.
		Need 5KG fert, for 1KG in ground.
	*/
	int nitrogen = 0;
	int phosphorus = 0;
	int potassium = 0;

	public Plant() {
		name = "Generic Plant";
	}

	// Immediately set health and energy to i.
	public void setHealth(int i) {
		health = i;
		energy = health;
	}

	public void setName(String n) {
		name = n;
	}

	public void setSize(int i) {
		size = i;
	}

	public void setXY(int setX, int setY) {
		x = setX;
		y = setY;
	}

}