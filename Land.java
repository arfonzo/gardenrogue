import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.security.SecureRandom;

/* Land
 *	Each Land object represents a 1x1 metre square on the Plot.
 */
public class Land {
	int plotX, plotY;

	int altitude, moisture, humidity, temperature;

	Plant plant;

	double soilPH;

	private static final Random RANDOM = new SecureRandom();

	public Land(int x, int y) {
		//SecureRandom random = new SecureRandom();
		//SecureRandom random = SecureRandom.getInstanceStrong();
		byte[] randValues = new byte[3];
		RANDOM.nextBytes(randValues);
		
		plotX = x;
		plotY = y;

		// Generate some random altitudes, for now.
		altitude = RANDOM.nextInt(10);

		// Generate some random moisture, for now.
		moisture = RANDOM.nextInt(10);

		float phVariance = ((float) RANDOM.nextInt(10))/10;
		//if (GardenRogue.debugMode) System.out.println("PH Variance: " + phVariance);
		if (RANDOM.nextInt(2) == 1) {
			// 50% chance to tend towards ericaceous instead of alkaline.
			phVariance *= -1;
		}

		soilPH = 7.0 + phVariance;

		// Does this Land have a Plant?
		if (RANDOM.nextInt(2) == 1) {
			//if (GardenRogue.debugMode) System.out.println("SecureRandom: " + new String(randValues));

			/*plant = new Plant();
			plant.setName("Plant " + new String(randValues));
			*/
			plant = new Fern();

			int h = RANDOM.nextInt(20) + 1;
			plant.setHealth(h);

			plant.setXY(plotX, plotY);

			plant.setSize(RANDOM.nextInt(1000) + 1);
		} else {
			plant = null;
		}

		//if (GardenRogue.debugMode) System.out.println("New Land Object created at Plot location " + plotX + "x" + plotY);
	}

	public int getAltitude() {
		return altitude;
	}
}